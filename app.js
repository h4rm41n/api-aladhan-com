var app = angular.module('app',[]);
app.service('Service', function($http){
	var base_url = "http://api.aladhan.com/"
	return {
		getBaseLocalStorage : function(){
			return $http.get('./json-file/city.json')
		},

		getLatLng : function(city){
			return $http.get(base_url+"cityInfo?city="+city+"&country=ID")
		},

		getTiming : function(data){
			let latitude =  data!==null ? data.data.latitude:"-8.6662282"
			let longitude = data!==null ? data.data.longitude:"116.5255946"
			let timezone =  data!==null ? data.data.timezone:"Asia/Makassar"
			
			let url_timing = "calendar?latitude="+latitude+"&longitude="+longitude+"&timezonestring="+timezone+"&method=2&year=2017"
			let url = base_url+url_timing
			return $http.get(url)
		}
	}

})

app.controller('ctrl', function($scope, Service){
	$scope.date = new Date()

	$scope.city = null

	Service.getBaseLocalStorage()
	.then(function(res){
		$scope.city = res.data
	});
	

	$scope.changeCity = function(city){
		Service.getLatLng(city.name)
		.then(function(res){
			Service.getTiming(res.data)
			.then(function(res){
				$scope.dataTiming = res.data.data
			})
		})
	};

	var reload = function(){
		Service.getTiming(data=null)
		.then(function(res){
			$scope.dataTiming = res.data.data
		})
	};

	reload();
})